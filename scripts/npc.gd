extends RigidBody

var a = false
var b = false

func _ready():
	randomize()
	$MeshInstance.material_override.albedo_color = Color.from_hsv(randf(), 0.5, 1, 1)

func _physics_process(delta):
	if randf() < 0.01:
		b = !b
	if b:
		apply_central_impulse(Vector3.FORWARD.rotated(Vector3.UP, rotation.y) * delta * 10)
	else: 
		apply_central_impulse(Vector3.FORWARD.rotated(Vector3.UP, rotation.y) * delta * -10)
	
	if randf() < 0.01:
		a = !a
	if a:
		apply_torque_impulse(Vector3(0, 2, 0) * delta)
	else:
		apply_torque_impulse(Vector3(0, -2, 0) * delta)



func _on_npc_body_entered(body):
	if body.name != "player":
		return
	body.kill()
