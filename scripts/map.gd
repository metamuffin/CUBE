extends Spatial

export var color: Color
const m_obstacle = preload("res://materials/obstacle.tres")
const m_ground = preload("res://materials/ground.tres")
const env = preload("res://materials/env.tres")

func _ready():
	m_obstacle.albedo_color = color.blend(Color(0.5,0.5,0.5,0.5))
	m_ground.albedo_color = color
	env.background_mode = 1
	env.background_color = Color.black
