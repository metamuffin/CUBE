extends Control

export var level: int = 0
export var fullres: bool = true

func _ready():
	load_level()
	update_res()

func _process(_delta):
	if Input.is_action_just_pressed("debug_fullres"):
		fullres = !fullres
		update_res()

func update_res():
	if fullres:
		Engine.target_fps = 60
		$viewport.size = OS.window_size
	else:
		Engine.target_fps = 15
		$viewport.size = Vector2(320, 240)
	$texture.expand = false
	$texture.expand = true


func invert_faces(g):
	for c in g.get_children():
		if "invert_faces" in c:
			c.invert_faces = true
		if "mesh" in c:
			if "flip_faces" in c.mesh:
				c.mesh = c.mesh.duplicate()
				c.mesh.flip_faces = true
		invert_faces(c)

func load_level():
	var g = get_node_or_null("viewport/level")
	if g != null: g.queue_free()
	yield(get_tree(), "idle_frame") # await deletion
	
	if level < 0:
		g = preload("res://scenes/maps/default.tscn").instance()
		g.color = Color.magenta
		invert_faces(g)
	elif level == 0:
		g = preload("res://scenes/maps/first.tscn").instance()
	elif level < 4:
		g = preload("res://scenes/maps/default.tscn").instance()
		if level == 1: g.color = Color.blue
		if level == 2: g.color = Color.green
		if level == 3: g.color = Color.orange
	elif level == 4:
		g = preload("res://scenes/maps/red.tscn").instance()
	
	g.name = "level"
	$viewport.add_child(g)

func win():
	level += 1
	load_level()
	
func death():
	level -= 1
	load_level()

