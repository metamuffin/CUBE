extends Spatial


func _ready():
	pass

var triggered = false
func _on_trigger_body_entered(body):
	if body.name != "player": return
	if triggered: return 
	triggered = true
	yield(get_tree().create_timer(2), "timeout")
	$animation.play("roll")

