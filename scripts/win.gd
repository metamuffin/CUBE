extends Spatial


func _ready():
	pass


func _on_Area_body_entered(body):
	if body.name != "player":
		return
	
	body.set_frozen(true)
	body.get_camera().mode = body.get_camera().CameraMode.FOLLOW
	
	$tween.interpolate_property(body, "global_transform:origin",
			body.global_transform.origin, global_transform.origin, 1,
			Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	$tween.start() 
	yield($tween, "tween_all_completed")

	$tween.interpolate_property(body, "global_transform:origin",
		body.global_transform.origin, global_transform.origin + Vector3(0,10,1), 1,
		Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	$tween.interpolate_property(body, "rotation",
		body.rotation, body.rotation + Vector3(0,10,0), 1,
		Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	$tween.start()
	$ringC.hide() 
	yield($tween, "tween_all_completed")
	
	$tween.interpolate_property(body.get_node("mesh"), "rotation",
		body.rotation, body.rotation + Vector3(0,2,2), 3,
		Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	$tween.start() 
	yield($tween, "tween_all_completed")
	
	get_tree().current_scene.win()


