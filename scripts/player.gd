extends RigidBody

var alive = true
var was_on_ground = false

func _ready():
	set_center_of_mass(Vector3.DOWN)

func set_frozen(b):
	mode = MODE_STATIC if b else MODE_RIGID

func get_camera():
	return $"../camera"

func _physics_process(delta):
	if Input.is_action_pressed("movement_forward"):
		apply_central_impulse(Vector3.FORWARD.rotated(Vector3.UP, global_transform.basis.get_euler().y) * delta * 20)
	if Input.is_action_pressed("movement_backward"):
		apply_central_impulse(Vector3.FORWARD.rotated(Vector3.UP, global_transform.basis.get_euler().y) * delta * -20)
	if Input.is_action_pressed("movement_left"):
		apply_torque_impulse(Vector3(0, 7, 0) * delta)
	if Input.is_action_pressed("movement_right"):
		apply_torque_impulse(Vector3(0, -7, 0) * delta)
	
	var rot_death = abs(rotation.x) > 1.5 or abs(rotation.z) > 1.5
	var on_ground = not get_world().direct_space_state.intersect_ray(
		global_transform.origin,
		global_transform.origin + Vector3.DOWN * (1 + sqrt(2) + 0.1),
		[self]
	).empty()
	var fell_down = global_transform.origin.y < -0.2
	if (rot_death and on_ground) or fell_down:
		self.kill()
	
	if was_on_ground != on_ground:
		if on_ground: set_center_of_mass(Vector3.DOWN)
		else: set_center_of_mass(Vector3.ZERO)
	was_on_ground = on_ground

func kill():
	if !alive: return 
	alive = false
	get_tree().paused = true
	$"../overlay/gameover".visible = true
	yield(get_tree().create_timer(3, true), "timeout")
	get_tree().paused = false
	get_tree().current_scene.death()


var mass_off = Vector3.ZERO
func set_center_of_mass(v):
	global_transform.origin += v - mass_off
	for c in get_children():
		c.transform.origin = -v
	mass_off = v

