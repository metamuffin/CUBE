extends Camera

var offset

enum CameraMode { BEHIND, FOLLOW }
var mode = CameraMode.BEHIND

func _ready():
	offset = global_transform.origin

func _process(delta):
	var player = $"../player/mesh".global_transform
	var player_pos = player.origin
		
	if mode == CameraMode.FOLLOW:
		var d = (player_pos + Vector3.UP * 8) - global_transform.origin
		global_transform.origin += d.normalized() * delta * (d.length() - 10)
	elif mode == CameraMode.BEHIND:
		var i = $"../player/camera_duck".get_overlapping_bodies()
		i.erase($"../player")
		var duck = not i.empty()
		var behind_off = Vector3.BACK.rotated(Vector3.UP, player.basis.get_euler().y) * 6
		var target_pos = player_pos + behind_off + Vector3.UP * (2 if duck else 8)
		var diff = target_pos - global_transform.origin
		global_transform.origin += diff.normalized() * delta * diff.length() * 1
				
	look_at(player_pos, Vector3.UP)

