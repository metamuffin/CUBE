extends RigidBody

var spawned = false
func _ready():
	mode = MODE_STATIC
	scale = Vector3(0, 0, 0)

func spawn(body):
	if body.name != "player": return
	if spawned: return
	spawned = true
	
	$tween.interpolate_property(self, "scale",
			Vector3(0, 0, 0), Vector3(1, 1, 1), 3,
			Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	$tween.start()
	yield($tween, "tween_all_completed")
	mode = MODE_RIGID

