extends Spatial

export var color: Color = Color(1,0, 0xf5 / 0xff)

func _ready():
	$"diamond/Cube".material_override = $"diamond/Cube".material_override.duplicate()
	$"diamond/Cube".material_override.albedo_color = color


func _process(delta):
	rotate_y(delta * 5)

func _on_Area_body_entered(body):
	if body.name == "player":
		queue_free()
