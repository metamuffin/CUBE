extends CSGTorus


func _ready():
	pass

var open = false
var moving = false
func _process(_delta):
	var should_open = get_tree().get_nodes_in_group("diamond").empty()
	
	if should_open != open && !moving:
		moving = true
		print("aa")
		var target_rot = Vector3(-PI * 0.2,0,0) if should_open else Vector3(0,0,0)
		$tween.interpolate_property(self, "rotation", self.rotation, target_rot, 2, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$tween.start()
		yield($tween,"tween_all_completed")
		
		open = should_open
		moving = false
