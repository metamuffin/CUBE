extends Area


func _ready():
	pass

func _on_Area_body_entered(body):
	if body.name != "player": return
	body.apply_central_impulse(Vector3(0, 11, -11))
	body.apply_torque_impulse(Vector3(1,1,1))
